import React from 'react'
import CardContainer from '../../components/CardContainer'

const ScraperView = () => {
  return (
    // Edit this as you like!
    <CardContainer height='30vh' width='50%' margin='2rem auto' padding='2rem' justifyContent='center' alignItems='center'>
      Hemos puesto esta plantilla a tu disposición.
      <br/>
      <br/>
      ¡Mucho éxito! ;)
    </CardContainer>
  )
}

export default ScraperView